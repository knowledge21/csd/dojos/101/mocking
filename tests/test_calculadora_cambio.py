from unittest import TestCase
from unittest.mock import MagicMock
from sistema_vendas.buscador_de_cotacao import BuscadorDeCotacao
import sistema_vendas.calculadora_cambio as calculadora_cambio

#CÓDIGO DE TESTE!
class TestCalculadoraCambio(TestCase):

    def teste_venda_de_0_euros_cotacao_qualquer_retorna_0_dolares(self):

        #ARRANGE
        data = '23-06-2020'
        venda = 0
        resultadoEsperado = 0

        #ACT
        resultadoCalculado = calculadora_cambio.converter_euro_para_dolar(venda, data)

        #ASSERT
        self.assertEqual(resultadoCalculado, resultadoEsperado)
